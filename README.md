# README #

## Introduction:  

This is a website I am creating for a client - Dr. Kurt Grimm. He is a Principal and Founder, Kingfisher Research and an ex-UBC professor.

Click on the following link to view the website: **[Dr.KurtGrimm](http://www.drkurtgrimm.com)**


## Technologies: ##

* HTML
* CSS/SASS
* Javascript
* Adobe Photoshop
* Invision

## Purpose: ##

* This is an exercise in creating a project based on client requirements and to gain experience in professionalism and working with clients.
* Exceed client's expectations.

## Procedure: ##

1. Browse to **[Website](http://www.drkurtgrimm.com)**



## Timestamp: ##

**October, 2015 – Jan, 2016