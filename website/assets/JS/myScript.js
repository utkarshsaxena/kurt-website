// Main JS for the website.

$(window).load(function() {
    
// Load images for the slide show after the page has loaded
    
    // Load the summary images of the top slide show
    $('.carousel-inner .summary-top').map(function(i,j){
        $(j).attr('src', $(j).attr('data-src'));
        $(j).removeAttr('data-src');    
    });
    
    // Load the summary images of the bottom slide show
    $('.carousel-inner .summary-bottom').map(function(i,j){
        $(j).attr('src', $(j).attr('data-src'));
        $(j).removeAttr('data-src');    
    });
    
    
// Bind click events on images to opening modals.
    $('.image-one').on('click', function(event){
        event.preventDefault();
        $('#image-one-slideshow').modal('show');
        $('.carousel-inner .slides-image-one').map(function(i,j){
            $(j).attr('src', $(j).attr('data-src'));
            $(j).removeAttr('data-src');    
        });
    });
    
    $('.image-two').on('click', function(event){
        event.preventDefault();
        $('#image-two-slideshow').modal('show');
        $('.carousel-inner .slides-image-two').map(function(i,j){
            $(j).attr('src', $(j).attr('data-src'));
            $(j).removeAttr('data-src');    
        });
    });
    
    $('.image-three').on('click', function(event){
        event.preventDefault();
        $('#image-three-slideshow').modal('show');
        $('.carousel-inner .slides-image-three').map(function(i,j){
            $(j).attr('src', $(j).attr('data-src'));
            $(j).removeAttr('data-src');    
        });
    });
    
    $('.image-four').on('click', function(event){
        event.preventDefault();
        $('#image-four-slideshow').modal('show');
        $('.carousel-inner .slides-image-four').map(function(i,j){
            $(j).attr('src', $(j).attr('data-src'));
            $(j).removeAttr('data-src');    
        });
    });
    
    // Pause all carousels
    $('#carousel-generic-1').carousel('pause');
    $('#carousel-generic-2').carousel('pause');
    $('#carousel-generic-3').carousel('pause');
    $('#carousel-generic-4').carousel('pause');
    

});